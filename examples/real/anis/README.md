# Anis

This application is from the geophysics domain and consists of 670 tasks performing tabulation of a complicated multidimensional function for solving an inverse problem.

- `anis100.plan` - small scale run
- `anis670.plan` - full run