# Vina

This application is from the life sciences domain and represents the virtual screening of a set of ligand molecules against the same protein using the molecular docking program Autodock Vina.

- `vina10.plan` - creates 10 tasks, use it with `files10.zip` which includes 10 different ligands
- `vina100.plan` - created 100 tasks, use it with on of `files1*.zip` files
- `vina1000.plan` - created 1000 tasks, use it with on of `files1*.zip` files

`files1*.zip` files include only one ligand file, so all tasks actually perform docking of the same pair of molecules (of course, in real life the zip should contain N different ligands):

- `files1-ex1.zip` - runs Vina with exhaustiveness parameter set to 1 (corresponds to short task run time)
- `files1-ex3.zip` - runs Vina with exhaustiveness parameter set to 3 (corresponds to medium task run time)
- `files1-ex8.zip` - runs Vina with exhaustiveness parameter set to 8 (corresponds to long task run time)